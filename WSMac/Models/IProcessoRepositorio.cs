﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSMac.Models
{
    interface IProcessoRepositorio
    {
        IEnumerable<Processo> GetAll();
        Processo Get(int id);
        Processo Add(Processo item);
    }
}
