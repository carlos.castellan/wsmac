﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSMac.Models
{
    public class Processo
    {
        public int Id { get; set; }
        public string caso { get; set; }
        public string cnj { get; set; }
        public string codigo_cliente { get; set; }
        public string status { get; set; }
        public string pasta { get; set; }
        public string advogado_interno { get; set; }
        public string advogado_externo { get; set; }
        public string advogado_parte_contraria { get; set; }
        public string nome_reclamante { get; set; }
        public string cpf_cnpj { get; set; }
        public string ds_tipo_testemunha { get; set; }
        public string ds_observacao { get; set; }
        public string DS_nome_Arquivo { get; set; }
        public string DS_Tipo_Arquivo { get; set; }
    }
}