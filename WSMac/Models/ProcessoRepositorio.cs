﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;

namespace WSMac.Models
{
    public class ProcessoRepositorio : IProcessoRepositorio
    {
        public int linhas;

        private List<Processo> processos = new List<Processo>();
        private int _nextId = 1;

        public ProcessoRepositorio() 
        {
        }

        public ProcessoRepositorio(string cnj, string caso, string pasta, string nome_reclamante)
        {

            string strConexao = "Server=magneto;Database=dlbca;User Id=rumpelstilzchen;Password=@#17le18b*c7@;;";
            string strSql;
            strSql = "proc_wsConsulta_Processo";

            using (SqlConnection connection = new SqlConnection(strConexao))
            {
                SqlCommand command = new SqlCommand(strSql, connection);
                
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = strSql;

                command.Parameters.Add("@nr_cnj", SqlDbType.VarChar, 200).Value = cnj;
                command.Parameters.Add("@pasta", SqlDbType.VarChar, 200).Value = pasta;
                command.Parameters.Add("@nome_reclamante", SqlDbType.VarChar, 200).Value = nome_reclamante;
                command.Parameters.Add("@cd_caso", SqlDbType.VarChar, 200).Value = caso;

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        linhas++;

                        Add(new Processo
                        {
                            caso = reader["caso"].ToString(),
                            cnj = reader["cnj"].ToString(),
                            codigo_cliente = reader["codigo_cliente"].ToString(),
                            status = reader["status"].ToString(),
                            pasta = reader["pasta"].ToString(),
                            advogado_interno = reader["advogado_interno"].ToString(),
                            advogado_externo = reader["advogado_externo"].ToString(),
                            advogado_parte_contraria = reader["advogado_parte_contraria"].ToString(),
                            nome_reclamante = reader["nome_reclamante"].ToString(),
                            cpf_cnpj = reader["cpf_cnpj"].ToString(),
                            ds_tipo_testemunha = reader["ds_tipo_testemunha"].ToString(),
                            ds_observacao = reader["ds_observacao"].ToString(),
                            DS_nome_Arquivo = reader["DS_nome_Arquivo"].ToString(),
                            DS_Tipo_Arquivo = reader["DS_Tipo_Arquivo"].ToString()
                        });
                    }
                }
            }



        }

        public Processo Add(Processo item)
        {
            if(item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            processos.Add(item);
            return item;
        }

        
        public Processo Get(int id)
        {
            return processos.Find(p => p.Id == id);
        }

        public IEnumerable<Processo> GetAll()
        {
            return processos;
        }

    }
}