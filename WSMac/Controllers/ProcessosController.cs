﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSMac.Models;
using System.Configuration;
using System.Web.Configuration;

namespace WSMac.Controllers
{
    public class ProcessosController : ApiController
    {
        IProcessoRepositorio repositorio;

        public IEnumerable<Processo> GetAllProcessos(string cnj, string caso, string pasta, string nome_reclamante)
        {

            ProcessoRepositorio repositorio = new ProcessoRepositorio();

            var headers = this.Request.Headers;
            if (checkAutentication(headers))
            {
                repositorio = new ProcessoRepositorio(cnj, caso, pasta, nome_reclamante);
//                repositorio.GetNone();
            }

            return repositorio.GetAll();

        }

        public Processo GetProcesso(int caso)
        {
            repositorio = new ProcessoRepositorio("", caso.ToString(), "", "");
            Processo item = repositorio.Get(caso);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        private bool checkAutentication(System.Net.Http.Headers.HttpRequestHeaders headers)
        {
            string username = ConfigurationManager.AppSettings["username_Mac"].ToString();
            string password = ConfigurationManager.AppSettings["password_Mac"].ToString();
            bool retorno = true;
            if (headers.Contains("username_Mac") && headers.Contains("password_Mac"))
            {
                if (headers.GetValues("username_Mac").ToArray()[0] != username || headers.GetValues("password_Mac").ToArray()[0] != password)
                {
                    retorno = false;
                }
            }
            else
            {
                retorno = false;
            }
            return retorno;
        }


    }
}
